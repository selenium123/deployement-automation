package com.Upgradation.Upgrade;

import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.Upgradation.BuisinessLib.Upgrade;
import com.Upgradation.GenericLib.Common;
import com.Upgradation.GenericLib.Driver;
import com.Upgradation.GenericLib.ExcelLIb;
import com.Upgradation.Logger.ExtLogger;
import com.Upgradation.Pagelib.UpgradeOrDeploy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;

public class UpgradeTest {

	ExtLogger logger = ExtLogger.getLogger(UpgradeOrDeploy.class);
	ExcelLIb elib = new ExcelLIb();
	public WebDriver driver;
	public Common common;

	
	String userName;
	String password;
	String url;
	String browser;
	Upgrade upgrade;

//	@BeforeClass
//	public void beforeClass(XmlTest x) {
//
//		userName = x.getParameter("username");
//		password = x.getParameter("password");		
//		browser = x.getParameter("browser");
//		System.out.println(userName);
//		System.out.println(password);		
//		System.out.println(browser);
//	}

	@BeforeMethod
	public void beforeMethod() {
		driver=Driver.getDriver("firefox");
		upgrade = new Upgrade(driver);
	}

	@Test
	public void deployement() throws InvalidFormatException, IOException, InterruptedException {	
		upgrade.pkgDeployUpgrade();
	}

	@AfterMethod
	public void configAfterMethod(XmlTest x, ITestResult t) throws IOException {
		boolean status = t.isSuccess();
		String testCaseName = t.getMethod().getMethodName();
		if (status) {
			logger.info("Test case " + testCaseName + " got passed.");
		} else {
			logger.info("Test case " + testCaseName + " got failled.");
			common.screenShots(testCaseName);

		}

	}

	@AfterClass
	public void afterClass() {
		System.out.println("Upgradation/Deployement process completed");
		logger.info("Upgradation/Deployement process completed");
	}

}
