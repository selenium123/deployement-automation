package com.Upgradation.BuisinessLib;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.Upgradation.GenericLib.Common;
import com.Upgradation.GenericLib.ExcelLIb;
import com.Upgradation.Logger.ExtLogger;
import com.Upgradation.Pagelib.DomainNamecreation;
import com.Upgradation.Pagelib.LoginToInstance;
import com.Upgradation.Pagelib.Permission;
import com.Upgradation.Pagelib.SetUpClicking;
import com.Upgradation.Pagelib.UpgradeOrDeploy;

/**
 * It performs all deployement and setup related operation.
 * @author Santosh
 *
 */
public class Upgrade {
	
	ExtLogger logger = ExtLogger.getLogger(UpgradeOrDeploy.class);
	ExcelLIb elib=new ExcelLIb();
	public WebDriver driver;
	public Common common;

	public Upgrade(WebDriver driver) {
		this.driver = driver;
		this.common = new Common(driver);
	}
	

	
	
	
	/**
	 * It performs all deployement and setup related operation.
	 * @param userName
	 * @param password
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public void pkgDeployUpgrade() throws InvalidFormatException, IOException, InterruptedException{
		
		UpgradeOrDeploy deploy=PageFactory.initElements(driver, UpgradeOrDeploy.class);
		LoginToInstance login=PageFactory.initElements(driver, LoginToInstance.class);
		DomainNamecreation domain=PageFactory.initElements(driver, DomainNamecreation.class);
		SetUpClicking setup= PageFactory.initElements(driver, SetUpClicking.class);
		Permission permission=PageFactory.initElements(driver, Permission.class);
		
				
		String packageName = elib.getexceldata("Deployement", 1, 0);
		String versionName = elib.getexceldata("Deployement", 1, 1).replace("v", "");
		String pkgUrl = elib.getexceldata("Deployement", 1, 2);		
		String domainName=elib.getexceldata("Deployement", 1,3 );	
		String siteName=elib.getexceldata("Deployement", 1, 4);
		String userName=elib.getexceldata("Deployement", 1, 5);
		String password=elib.getexceldata("Deployement", 1, 6);		
		

		/*Will login to the instance.*/
		login.login(userName,password, pkgUrl);
		
		/*Will deploy/Upgrade the desired package.*/
		deploy.deployement(versionName, versionName, packageName);
		
		/*Will create domain name.*/
		domain.createDomainName(domainName);
		
		/*Will create site.*/
		domain.siteCreation(siteName);
		
		/*Will set home page.*/
		setup.homePageLayout();
		
		/*Will create TargetRecruit Admin tool component if required.*/
		setup.targetRecruitAdminTool();
		
		/*Will click on all setup links to perform setup operation.*/
		setup.clickingOnSetUp();	
		
		/*Will provide the object level and field level permission.*/
		permission.permissionSet(siteName);
		
		
	}

}
