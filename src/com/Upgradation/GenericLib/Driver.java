package com.Upgradation.GenericLib;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
  
/**
 * WebDriver driver class.
 * class provide wendriver driver libraries , which can be used to launch the different browsers like ,firefox,chrome etc
 *
 * @author Santosh
 *
 */

public class Driver {


	public static WebDriver driver;

	public static WebDriver getDriver(String browser){
		
		if (browser.equalsIgnoreCase("firefox")){			
			driver = new FirefoxDriver();
			
		}else if (browser.equalsIgnoreCase("ie")){
			DesiredCapabilities capability = DesiredCapabilities.internetExplorer(); 
			
	           capability = DesiredCapabilities.internetExplorer();
	            capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	            capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			System.setProperty("webdriver.ie.driver", "C:\\Users\\hunasd\\TargetRecruit\\TRFameWork\\resources\\IEDriverServer.exe");
			
			driver = new InternetExplorerDriver();
		}else if (browser.equalsIgnoreCase("chrome")) {
			//File file = new File("resources/chromedriver.exe");
			String userDir = System.getProperty("user.dir");
			 String  filePath = userDir+"\\resources\\chromedriver.exe";

			System.setProperty("webdriver.chrome.driver", filePath);

			//System.setProperty("webdriver.chrome.driver", "C:\\Users\\hunasd\\TargetRecruit\\TRFameWork\\resources\\chromedriver.exe");

	
			driver = new ChromeDriver();
		}else{
			
			DesiredCapabilities ca = DesiredCapabilities.safari();
			driver = new SafariDriver();
		}
		
		return driver;
	}
}
