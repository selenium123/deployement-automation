package com.Upgradation.Pagelib;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import com.Upgradation.GenericLib.Common;
import com.Upgradation.GenericLib.ExcelLIb;
import com.Upgradation.Logger.ExtLogger;
/***
 * Will set Object level permission and field level permission.
 * @author Santosh
 *
 */
public class Permission {
	
	ExtLogger logger = ExtLogger.getLogger(UpgradeOrDeploy.class);
	ExcelLIb elib=new ExcelLIb();
	public WebDriver driver;
	public Common common;

	public Permission(WebDriver driver) {
		this.driver = driver;
		this.common = new Common(driver);
	}
	
	
	
	
	@FindBy(xpath="//a[normalize-space(text())='Public Access Settings']")
	private WebElement publicAccessSettingBtn;
	
	@FindBy(xpath="//tr[td[h3[normalize-space(text())='Enabled Apex Class Access']]]//input[@name='edit']")
	private WebElement apexClassAccessEditbutton;
	
	@FindBy(xpath="//select[@id='duel_select_0']")
	private WebElement apexClassSelectBox;
	
	@FindBy(xpath="//img[@class='rightArrowIcon']")
	private WebElement rightArrowImg;
	
	@FindBy(xpath="//input[normalize-space(@name)='save']")
	private WebElement savebuton;
	
	@FindBy(xpath="//tr[td[h3[normalize-space(text())='Enabled Visualforce Page Access']]]//input[@name='edit']")
	private WebElement visualForcePageAccessEditbutton;		

	@FindAll({@FindBy(xpath="//input[contains(@name,'ModifyAll')]")})
	private WebElement modifyAllCheckBox;
	
	@FindBy(xpath="//td[@id='topButtonRow']//input[@name='edit']")
	private WebElement pageEditButton;
	
	@FindBy(xpath="//td[@id='topButtonRow']//input[@name='save']")
	private WebElement objectSaveButton;	
	
	@FindBy(xpath ="//div[@class='pbHeader']//input[normalize-space(@value)='Edit']")
	private WebElement editprofilebutton;
	
	@FindBy(xpath="//div[@class='pbHeader']//input[@name='save']")
	private WebElement fieldSaveButton;
	
	@FindBy(xpath="(//tr[th[normalize-space(text())='Accounts']]//input[contains(@name,'Create')])[1]")
	private WebElement accountCreateCheckbox;
	
	@FindBy(xpath="(//tr[th[normalize-space(text())='Contacts']]//input[contains(@name,'Create')])[1]")
	private WebElement contactCreateCheckBox;
	
	@FindBy(xpath="(//tr[th[normalize-space(text())='Contracts']]//input[contains(@name,'Create')])[1]")
	private WebElement contractCreateCheckBox;
	
	@FindBy(xpath="(//tr[th[normalize-space(text())='Documents']]//input[contains(@name,'Create')])[1]")
	private WebElement documentCreateCheckBox;
	
	
	/***
	 * Will set Object level and field level Permission.
	 */
	public void permissionSet(String siteName){
		logger.info("Will set Object and field level permissions. ");
		DomainNamecreation domain = PageFactory.initElements(driver, DomainNamecreation.class);
		domain.siteLinkClicking();
		common.waitForPageToLoad();
		driver.findElement(By.xpath("//a[normalize-space(text())='"+siteName+"']")).click();
		common.waitForPageToLoad();
		publicAccessSettingBtn.click();
		common.waitForPageToLoad();
		apexClassAccessEditbutton.click();
		common.waitForPageToLoad();
		Select sel= new Select(apexClassSelectBox);			
		while(sel.getOptions().size()>0){			
			sel.selectByIndex(0);
			if(sel.getOptions().get(0).getText().equals("--None--")){					
				break;
			}
			rightArrowImg.click();
		}		
		
		savebuton.click();
		common.waitForPageToLoad();
		visualForcePageAccessEditbutton.click();
		common.waitForPageToLoad();
		Select sel1= new Select(apexClassSelectBox);		
		while(sel.getOptions().size()>0){
			
			sel1.selectByIndex(0);
			if(sel1.getOptions().get(0).getText().equals("--None--")){					
				break;
			}
			rightArrowImg.click();
		}
		savebuton.click();
		common.waitForPageToLoad();
		pageEditButton.click();
		common.waitForPageToLoad();
		/*Object level permission.*/
		if(accountCreateCheckbox.isSelected()==false){
			accountCreateCheckbox.click();
		}
		if(contactCreateCheckBox.isSelected()==false){
			contactCreateCheckBox.click();
		}
		if(contractCreateCheckBox.isSelected()==false){
			contractCreateCheckBox.click();
		}
		if(documentCreateCheckBox.isSelected()==false){
			documentCreateCheckBox.click();
		}
		for(int i=1;i<=driver.findElements(By.xpath("//input[contains(@name,'ModifyAll')]")).size();i++){
			
			boolean flag=driver.findElement(By.xpath("(//input[contains(@name,'ModifyAll')])["+i+"]")).isSelected();
			boolean create=driver.findElement(By.xpath("(//div[h3[normalize-space(text())='Custom Object Permissions']]/following-sibling::div[1]//input[contains(@name,'Create')])["+i+"]")).isSelected();
			if(flag==false){
				driver.findElement(By.xpath("(//input[contains(@name,'ModifyAll')])["+i+"]")).click();
			}
			if(create==false){
				driver.findElement(By.xpath("(//div[h3[normalize-space(text())='Custom Object Permissions']]/following-sibling::div[1]//input[contains(@name,'Create')])["+i+"]")).click();
			}
			if(driver.findElement(By.xpath("(//td[table[tbody[tr[td[input[contains(@name,'ModifyAll')]]]]]]/preceding-sibling::th)["+i+"]")).getText().contains("Competency Details")){
				if(driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).isSelected()){
					driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).click();
				}
				
			}else if (driver.findElement(By.xpath("(//td[table[tbody[tr[td[input[contains(@name,'ModifyAll')]]]]]]/preceding-sibling::th)["+i+"]")).getText().contains("Educational History")) {
				if(driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).isSelected()){
					driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).click();
				}
			}else if (driver.findElement(By.xpath("(//td[table[tbody[tr[td[input[contains(@name,'ModifyAll')]]]]]]/preceding-sibling::th)["+i+"]")).getText().contains("Housing")) {
				if(driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).isSelected()){
					driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).click();
				}
			}else if (driver.findElement(By.xpath("(//td[table[tbody[tr[td[input[contains(@name,'ModifyAll')]]]]]]/preceding-sibling::th)["+i+"]")).getText().contains("Job Applicants")) {
				if(driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).isSelected()){
					driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).click();
				}
			}else if (driver.findElement(By.xpath("(//td[table[tbody[tr[td[input[contains(@name,'ModifyAll')]]]]]]/preceding-sibling::th)["+i+"]")).getText().contains("Jobs")) {
				if(driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).isSelected()){
					driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).click();
				}
			}else if (driver.findElement(By.xpath("(//td[table[tbody[tr[td[input[contains(@name,'ModifyAll')]]]]]]/preceding-sibling::th)["+i+"]")).getText().contains("Placements")) {
				if(driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).isSelected()){
					driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).click();
				}
			}else if (driver.findElement(By.xpath("(//td[table[tbody[tr[td[input[contains(@name,'ModifyAll')]]]]]]/preceding-sibling::th)["+i+"]")).getText().contains("Questionnaire Responses")) {
				if(driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).isSelected()){
					driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).click();
				}
			}else if (driver.findElement(By.xpath("(//td[table[tbody[tr[td[input[contains(@name,'ModifyAll')]]]]]]/preceding-sibling::th)["+i+"]")).getText().contains("Questions")) {
				if(driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).isSelected()){
					driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).click();
				}
			}
			else if (driver.findElement(By.xpath("(//td[table[tbody[tr[td[input[contains(@name,'ModifyAll')]]]]]]/preceding-sibling::th)["+i+"]")).getText().contains("Skills")) {
				if(driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).isSelected()){
					driver.findElement(By.xpath("(//input[contains(@name,'ViewAll')])["+i+"]")).click();
				}
			}
				
		}
		objectSaveButton.click();
		
		/*Field level permission.*/
		
		for(int j=1;j<=driver.findElements(By.xpath("//a[normalize-space(text())='View']")).size();j++){
			
			Actions act = new Actions(driver);			
			act.contextClick(driver.findElement(By.xpath("(//a[normalize-space(text())='View'])["+j+"]"))).perform();
			act.sendKeys("w").perform();
			common.waitForPageToLoad();
			Set<String> set2 = driver.getWindowHandles();
			Iterator<String> it2 = set2.iterator();
			String parentid2 = it2.next();
			String childid2 = it2.next();
			driver.switchTo().window(childid2);
			editprofilebutton.click();
			for(int k=1;k<=driver.findElements(By.xpath("//input[contains(@name,'edit')]")).size();k++){
				if(driver.findElement(By.xpath("(//input[contains(@name,'edit')])["+k+"]")).isSelected()==false){
					driver.findElement(By.xpath("(//input[contains(@name,'edit')])["+k+"]")).click();
				}
			}
			fieldSaveButton.click();
			common.waitForPageToLoad();
			driver.close();
			driver.switchTo().window(parentid2);
		}
		logger.info("Object level and Field level permission is provided.");
	}

}
