package com.Upgradation.Pagelib;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.Upgradation.GenericLib.Common;
import com.Upgradation.GenericLib.Driver;
import com.Upgradation.GenericLib.ExcelLIb;
import com.Upgradation.Logger.ExtLogger;

public class LoginToInstance {
	
	ExtLogger logger = ExtLogger.getLogger(UpgradeOrDeploy.class);
	ExcelLIb elib=new ExcelLIb();
	public WebDriver driver;
	public Common common;

	public LoginToInstance(WebDriver driver) {
		this.driver = driver;
		this.common = new Common(driver);
	}
	
	
	@FindBy(name = "username")
	private WebElement userNameEdt;

	@FindBy(id = "password")
	private WebElement passwordEdt;

	@FindBy(id = "Login")
	private WebElement loginBtn;
	
	
	public void login(String userName,String password,String url){
				
		driver.get(url);
		driver.manage().window().maximize();
		common.waitForWebElementPresent(userNameEdt);
		userNameEdt.sendKeys(userName);
		passwordEdt.sendKeys(password);
		loginBtn.click();
		common.waitForPageToLoad();
		
		
	}

}
