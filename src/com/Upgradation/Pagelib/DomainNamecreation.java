package com.Upgradation.Pagelib;

import java.util.Iterator;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.Upgradation.GenericLib.Common;
import com.Upgradation.Logger.ExtLogger;


/**
 * It contains alll domain name creation and site creation related methods in it.
 * @author Santosh
 *
 */
public class DomainNamecreation {
	
	ExtLogger logger = ExtLogger.getLogger(UpgradeOrDeploy.class);
	public WebDriver driver;
	public Common common;
	boolean flag= false;
	boolean flag2=true;

	public DomainNamecreation(WebDriver driver) {
		this.driver = driver;
		this.common = new Common(driver);
	}
	
	
	boolean domain= false;
	@FindBy(xpath = "//div[@id='userNav-arrow']")
	private WebElement userNavArrowLink;

	@FindBy(xpath = "//div/a[text()='Setup']")
	private WebElement setUpLink;
	
	@FindBy(xpath="//input[@id='setupSearch']")
	private WebElement setupSearchEdit;
	
	@FindBy(xpath="//a[normalize-space(text())='Sites']")
	private WebElement sitesLink;
	
	@FindBy(xpath="//strong[contains(text(),'http://')]//input")
	private WebElement domainNameEdit;
	
	@FindBy(xpath="//input[@value='Check Availability']")
	private WebElement checkAvailabilityBtn;
	
	@FindBy(xpath="//strong[contains(text(),'Success:')]")
	private WebElement successMessage;
	
	@FindBy(xpath="//div[label[contains(text(),'I have read and accepted the Force.com')]]//input[@type='checkbox']")
	private WebElement acceptCheckbox;
	
	@FindBy(xpath="//input[@value='Register My Force.com Domain']")
	private WebElement registerBtn;
	
	@FindBy(xpath="//div[contains(text(),'Your Force.com domain name')]")
	private WebElement domainNameText;
	
	@FindBy(xpath="//div[contains(text(),'Your Force.com domain name')]//strong")
	private WebElement domainNameValue;
	
	@FindBy(xpath="//input[@value='New']")
	private WebElement newButton;
	
	@FindBy(xpath="//th[label[normalize-space(text())='Site Label']]/following-sibling::td//input")
	private WebElement siteLabelEdit;
	
	@FindBy(xpath="//th[label[normalize-space(text())='Active Site Home Page']]/following-sibling::td//img")
	private WebElement activeSiteHomePageImageLink;
	
	@FindBy(xpath = "//input[@name='lksrch']")
	private WebElement lookupSearchEdt;

	@FindBy(xpath = "//input[@name='go']")
	private WebElement goBtn;
	
	@FindBy(xpath="//a[normalize-space(text())='CMSLayout']")
	private WebElement cmsLayoutLink;
	
	@FindBy(xpath="//div[@class='pbBottomButtons']//input[normalize-space(@value)='Save']")
	private WebElement siteSaveButton;
	
	@FindBy(xpath="//a[contains(text(),'Back to List:')]")
	private WebElement backToListSiteLink;
	
	@FindBy(xpath="//th[label[normalize-space(text())='Active']]/following-sibling::td//input")
	private WebElement activeCheckBox;
	
	@FindBy(xpath="//div[normalize-space(text())='Organization Administration Locked']")
	private WebElement adminLockedError;
	
	
	
	/**
	 * Will Click on Sites link to navigate site page.
	 */
	public void siteLinkClicking(){
		common.waitForWebElementPresent(userNavArrowLink);
		boolean flag1 = false;
		try {
			flag1 = setUpLink.isDisplayed();
		} catch (NoSuchElementException n) {

		}

		if (flag1) {
			setUpLink.click();
		} else {
			userNavArrowLink.click();
			setUpLink.click();
		}
		
		setupSearchEdit.clear();
		setupSearchEdit.sendKeys("Sites");
		
		common.waitForWebElementPresent(sitesLink);
		sitesLink.click();
	}
	
	/**
	 * Will create domain name.
	 * @param domainName
	 */
	public void createDomainName(String domainName){
		
		logger.info("Will create domain name.");
		siteLinkClicking();
		
		domainNameEdit.sendKeys(domainName);
		checkAvailabilityBtn.click();
		
		try {
			flag=successMessage.isDisplayed();
			System.out.println(successMessage.getText());
			logger.info(successMessage.getText());
		} catch (NoSuchElementException e) {
			
			Assert.assertTrue(flag, "Failled in creating Domain Name.");
			
		}
		
		acceptCheckbox.click();
		registerBtn.click();
		common.acceptAlert();
		
		try {
			domain=domainNameText.isDisplayed();
			System.out.println("Domain Name created successfully.");
			logger.info("Domain Name created successfully.");
			System.out.println("Your Force.com domain name is"+domainNameValue.getText() );
			
			
		} catch (Exception e) {
			Assert.assertTrue(domain, "Failled to create Domain Name");
		}
		logger.info("Domain name is created successfully.");
		
		
	}
	
	/**
	 * Will create Site.
	 * @throws InterruptedException 
	 */
	public void siteCreation(String siteName) throws InterruptedException {
		logger.info("Will create site.");
		while (flag2 == true) {
			common.refreshPage();
			common.handleUnexpectedAlert();
			siteLinkClicking();
			common.waitForWebElementPresent(newButton);
			newButton.click();
			common.waitForPageToLoad();
			siteLabelEdit.sendKeys(siteName);
			activeCheckBox.click();
			activeSiteHomePageImageLink.click();
			common.waitForPageToLoad();
			Set<String> set2 = driver.getWindowHandles();
			Iterator<String> it2 = set2.iterator();
			String parentid2 = it2.next();
			String childid2 = it2.next();
			driver.switchTo().window(childid2);

			driver.switchTo().frame("searchFrame");
			lookupSearchEdt.sendKeys("CMSLayout");
			goBtn.click();
			common.waitForPageToLoad();
			driver.switchTo().defaultContent();

			common.waitForPageToLoad();
			driver.switchTo().frame("resultsFrame");
			common.waitForWebElementPresent(cmsLayoutLink);
			cmsLayoutLink.click();
			driver.switchTo().window(parentid2);
			common.waitForWebElementPresent(siteSaveButton);
			siteSaveButton.click();
			common.waitForPageToLoad();
			try {
				flag2= adminLockedError.isDisplayed();
				System.out.println("site created ? = " + flag);
				
			} catch (Exception e) {
				System.out.println("site created ? = " + flag);
				flag2 = false;
			}
		}
		backToListSiteLink.click();
		common.waitForPageToLoad();
		try {
			driver.findElement(By.xpath("//a[normalize-space(text())='" + siteName + "']")).isDisplayed();
			System.out.println("Site is created successfully.");
			logger.info("Site is created successfully.");
		} catch (Exception e) {
			Assert.assertTrue(false, "Site creation failled.");
		}

		logger.info("Site is created successfully.");

	}

}
