package com.Upgradation.Pagelib;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import com.Upgradation.GenericLib.Common;
import com.Upgradation.GenericLib.ExcelLIb;
import com.Upgradation.Logger.ExtLogger;


/***
 * Will arrange all set related links under TargetrecruitToll and perform setUp operation.
 * @author Santosh
 *
 */
public class SetUpClicking {
	
	ExtLogger logger = ExtLogger.getLogger(UpgradeOrDeploy.class);
	ExcelLIb elib=new ExcelLIb();
	public WebDriver driver;
	public Common common;

	public SetUpClicking(WebDriver driver) {
		this.driver = driver;
		this.common = new Common(driver);
	}
	
	
	boolean domain= false;
	boolean availability=false;
	
	
	@FindBy(xpath = "//div[@id='userNav-arrow']")
	private WebElement userNavArrowLink;

	@FindBy(xpath = "//div/a[text()='Setup']")
	private WebElement setUpLink;
	
	@FindBy(xpath="//input[@id='setupSearch']")
	private WebElement setupSearchEdit;
	
	@FindBy(xpath="//a[normalize-space(text())='Home Page Components']")
	private WebElement homePageComponent;
	
	@FindBy(xpath="//input[normalize-space(@name)='new']")
	private WebElement newButton;
	
	@FindBy(xpath="//input[normalize-space(@value)='Next']")
	private WebElement nextButon;
	
	@FindBy(xpath="//td[label[normalize-space(text())='Name']]/following-sibling::td//input")
	private WebElement nameEdit;
	
	@FindBy(xpath="//div[@class='pbTopButtons']/input[normalize-space(@value)='Next']")
	private WebElement topNextButton;
	
	@FindBy(xpath="//select[@id='Body_select_0']")
	private WebElement linkSelectFrom;
	
	@FindBy(xpath="//a[@id='Body_select_0_right']/img")
	private WebElement rightArrow;
	
	@FindBy(xpath="//div[@class='pbTopButtons']/input[@name='save']")
	private WebElement savebutton;
	
	@FindBy(xpath="//input[normalize-space(@value)='Save & Assign']")
	private WebElement saveAssignButtonLayout;
	
	@FindBy(xpath="//div/a[normalize-space(text())='Home Page Layouts']")
	private WebElement homePageLayoutLink;
	
	@FindBy(xpath="//th[a[normalize-space(text())='TargetRecruit Admin']]/preceding-sibling::td//a[text()='Edit']")
	private WebElement targetrecruitAdminEditLink;
	
	@FindBy(xpath="//td[label[normalize-space(text())='Targetrecruit Admin Tool']]/following-sibling::td[1]//input")
	private WebElement targetRecruitAdminToolChkBox;
	
	@FindBy(xpath="//td[label[normalize-space(text())='System Administrator']]/following-sibling::td//select")
	private WebElement systemAdminSelectBox;
	
	@FindBy(xpath="//td[@id='bottomButtonRow']/input[@name='save']")
	private WebElement saveAssignPagebtn;
	
	@FindBy(xpath="//a/img[@title='All Tabs']")
	private WebElement allTabLink;
	
	@FindBy(xpath="//td/a[normalize-space(text())='Home']")
	private WebElement homeLink;
	
	@FindBy(xpath="//li/a[normalize-space(text())='Setup']")
	private WebElement setupHomeLink;
	
	@FindBy(xpath="//li/a[normalize-space(text())='Candidate Portal Setup']")
	private WebElement candidatePortalSetupLink;
	
	@FindBy(xpath="//li/a[normalize-space(text())='Client Portal Setup']")
	private WebElement clientPortalSetuplink;
	
	@FindBy(xpath="//li/a[normalize-space(text())='TimeSheet Setup']")
	private WebElement timesheetSetupLink;
	
	@FindBy(xpath="//input[@value='Setup']")
	private WebElement setupButton;
	
	@FindBy(xpath="//div[normalize-space(text())='Configuration setup has been successfully completed.']")
	private WebElement setupSuccessfulMesage;
	
	@FindBy(xpath="//input[normalize-space(@value)='Vendor Setup']")
	private WebElement vendorSetupButton;
	
	@FindBy(xpath="//input[normalize-space(@value)='Setup New Candidate Portal']")
	private WebElement newCandidatePortalSetupButton;
	
	@FindBy(xpath="//input[normalize-space(@value)='Client Portal Setup']")
	private WebElement clientPortalSetupButon;
	
	@FindBy(xpath="//td/a[normalize-space(text())='Projects']")
	private WebElement projectLink;
	
	@FindBy(xpath="//input[@name='go']")
	private WebElement gobuton;
	
	@FindBy(xpath="//input[normalize-space(@value)='Setup']")
	private WebElement projectSetupButton;
	
	
	/**
	 * Will check the availability of the webelement.
	 * @param webelement
	 * @return
	 */
	public boolean checkWebelementAvailability(WebElement webelement){
		try {
			availability=webelement.isDisplayed();
		} catch (NoSuchElementException e) {
			// TODO: handle exception
		}
		return availability;
	}
	/***
	 * Will create TargetRecruit Admin Tool and arrange all setup related links in it.
	 */
	public void targetRecruitAdminTool() {
		logger.info("If required it will create TargetRecruit Admin Tool component");			
		navigateToHome();
		boolean setupavailability = checkWebelementAvailability(setupHomeLink);
		boolean candidatePortalSetupAvailability = checkWebelementAvailability(candidatePortalSetupLink);
		boolean clientPortalAvailability = checkWebelementAvailability(clientPortalSetuplink);
		boolean timesheetSetupAvailability = checkWebelementAvailability(timesheetSetupLink);
		
		System.out.println(setupavailability);
		System.out.println(candidatePortalSetupAvailability);
		System.out.println(clientPortalAvailability);
		System.out.println(timesheetSetupAvailability);
		
		if (setupavailability == false || candidatePortalSetupAvailability == false || clientPortalAvailability == false
				|| timesheetSetupAvailability == false) {
			logger.info("Will create TargetRecruit Admin Tool component");	
			common.waitForWebElementPresent(userNavArrowLink);
			boolean flag1 = false;
			try {
				flag1 = setUpLink.isDisplayed();
			} catch (NoSuchElementException n) {

			}

			if (flag1) {
				setUpLink.click();
			} else {
				userNavArrowLink.click();
				setUpLink.click();
			}

			setupSearchEdit.clear();
			setupSearchEdit.sendKeys("Home Page Components");

			common.waitForWebElementPresent(homePageComponent);
			homePageComponent.click();

			common.waitForPageToLoad();

			newButton.click();
			common.waitForPageToLoad();
			nextButon.click();

			nameEdit.sendKeys("Targetrecruit Admin Tool");
			topNextButton.click();
			common.waitForPageToLoad();
			
			/*ADD setup select*/
			
			Select sel = new Select(linkSelectFrom);
			
			if(setupavailability == false){
				sel.selectByVisibleText("Setup");
			}
			if (timesheetSetupAvailability == false) {
				sel.selectByVisibleText("TimeSheet_Setup");
			}
			if (candidatePortalSetupAvailability == false) {
				sel.selectByVisibleText("Candidate_Portal_Setup");
			}
			if (clientPortalAvailability == false) {
				sel.selectByVisibleText("Client_Portal_Setup");
			}

			rightArrow.click();

			savebutton.click();
			homePageLayout();

			logger.info("Created TargetRecruit Admin Tool Component successfully.");
		}
	}
	/***
	 * Will navigate to Home page.
	 */
	public void navigateToHome(){
		allTabLink.click();
		common.waitForPageToLoad();
		homeLink.click();
		common.waitForPageToLoad();
		
	}
	
	/**
	 * Will bring the targetRecruit Tool companent to homepage and will provide admin access to the user.
	 */
	public void homePageLayout(){
		logger.info("Will set home page layout.");
		
		common.waitForWebElementPresent(userNavArrowLink);
		boolean flag1 = false;
		try {
			flag1 = setUpLink.isDisplayed();
		} catch (NoSuchElementException n) {

		}

		if (flag1) {
			setUpLink.click();
		} else {
			userNavArrowLink.click();
			setUpLink.click();
		}

		setupSearchEdit.clear();
		setupSearchEdit.sendKeys("Home Page Layouts");
		common.waitForWebElementPresent(homePageLayoutLink);
		homePageLayoutLink.click();
		common.waitForPageToLoad();
		targetrecruitAdminEditLink.click();
		common.waitForPageToLoad();
		try {			
			if (targetRecruitAdminToolChkBox.isSelected() == false) {
				targetRecruitAdminToolChkBox.click();
			}
		} catch (NoSuchElementException n) {
			
		}
		
		nextButon.click();
		common.waitForPageToLoad();
		saveAssignButtonLayout.click();
		common.waitForPageToLoad();
		common.select(systemAdminSelectBox, "TargetRecruit Admin");
		saveAssignPagebtn.click();
		common.waitForPageToLoad();	
		logger.info("home page is setted up.");
		
	}
	
	/**
	 * Will perform all setup operation.
	 */
	public void clickingOnSetUp(){
		logger.info("Will start Setup process.");
		/*Global Setup*/
		for(int i=1;i<=2;i++){
			navigateToHome();
			setupHomeLink.click();
			common.waitForPageToLoad();
			setupButton.click();
			common.waitForWebElementPresent(setupSuccessfulMesage);
			if(setupSuccessfulMesage.isDisplayed()){
				System.out.println("Setup is done ");
			}
			
		}
		/*Candidate portal Setup*/
		for(int j=1;j<=2;j++){
			navigateToHome();
			candidatePortalSetupLink.click();
			common.waitForPageToLoad();
			setupButton.click();
			common.waitForWebElementPresent(setupSuccessfulMesage);
			if(setupSuccessfulMesage.isDisplayed()){
				System.out.println("Candidate portalSetup is done ");
			}
		}
		
		/*Vendor portal Setup*/
		for(int j=1;j<=2;j++){
			navigateToHome();
			candidatePortalSetupLink.click();
			common.waitForPageToLoad();
			vendorSetupButton.click();
			common.waitForWebElementPresent(setupSuccessfulMesage);
			if(setupSuccessfulMesage.isDisplayed()){
				System.out.println("Vendor Portal Setup is done ");
			}
		
		}
		
		/*New candidate portal Setup*/
		for(int j=1;j<=2;j++){
			navigateToHome();
			candidatePortalSetupLink.click();
			common.waitForPageToLoad();
			newCandidatePortalSetupButton.click();
			common.waitForWebElementPresent(setupSuccessfulMesage);
			if(setupSuccessfulMesage.isDisplayed()){
				System.out.println("New candidate portal Setup is done ");
			}
		
		}
		
		/*Client Portal Setup*/
		for(int j=1;j<=2;j++){
			navigateToHome();
			clientPortalSetuplink.click();
			common.waitForPageToLoad();
			clientPortalSetupButon.click();
			common.waitForPageToLoad();
			if(setupSuccessfulMesage.isDisplayed()){
				System.out.println("New candidate portal Setup is done ");
			}
		
		}
		
		/*Timesheet Setup*/
		for(int j=1;j<=2;j++){
			navigateToHome();
			timesheetSetupLink.click();
			setupButton.click();
			common.waitForPageToLoad();
			common.waitForPageToLoad();
			if(setupSuccessfulMesage.isDisplayed()){
				System.out.println("New candidate portal Setup is done ");
			}
		
		}
		
//		/*Project setup*/
//		allTabLink.click();
//		common.waitForPageToLoad();
//		projectLink.click();
//		common.waitForPageToLoad();
//		gobuton.click();
//		common.waitForPageToLoad();
//		projectSetupButton.click();
//		common.waitForPageToLoad();
//		Set<String> set = driver.getWindowHandles();
//		Iterator<String> it = set.iterator();
//		String parentid = it.next();
//		String childid = it.next();
//		driver.switchTo().window(childid);
		
		
		
		logger.info("Setup process is completed susccessfully.");
		
	}
	

}
