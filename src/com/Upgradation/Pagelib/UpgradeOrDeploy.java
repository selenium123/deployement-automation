package com.Upgradation.Pagelib;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


import com.Upgradation.GenericLib.Common;
import com.Upgradation.Logger.ExtLogger;

/**
 * It contains all deployement /Upgradation related methods.
 * @author Santosh
 *
 */
public class UpgradeOrDeploy {
	ExtLogger logger = ExtLogger.getLogger(UpgradeOrDeploy.class);
	public WebDriver driver;
	public Common common;

	public UpgradeOrDeploy(WebDriver driver) {
		this.driver = driver;
		this.common = new Common(driver);
	}

	boolean flag = false;
	boolean upgraded = false;
	int count = 0;

	@FindBy(xpath = "//div[text()='Install for All Users']//input")
	private WebElement allUserradio;

	@FindBy(xpath = "//button/span[text()='Install']")
	private WebElement installButon;

	@FindBy(xpath = "//button/span[text()='Upgrade']")
	private WebElement upgradeButton;

	@FindBy(xpath = "//div[normalize-space(text())='Version Name']/following-sibling::div")
	private WebElement versionNameValue;

	@FindBy(xpath = "//div[normalize-space(text())='Version Number']/following-sibling::div")
	private WebElement versionNumberValue;

	@FindBy(xpath = "//div[contains(text(),'grant access')]/input[@type='checkbox']")
	private WebElement grantAccessCheckBox;

	@FindBy(xpath = "//button/span[text()='Continue']")
	private WebElement continueButton;

	@FindBy(xpath = "//span[normalize-space(text())='This app is taking a long time']")
	private WebElement longTimeToInstallText;

	@FindBy(xpath = "//button/span[text()='Done']")
	private WebElement doneButton;
	
	
	/***
	 * This will deploy/ Upgarade the package.
	 * @param versionName
	 * @param versionNumber
	 * @param packageName
	 */
	public void deployement(String versionName, String versionNumber, String packageName) {
		logger.info("Deployment/Upgradation process will start.");

		allUserradio.click();
		if (versionName.equals(versionNameValue.getText()) && versionNumber.equals(versionNumberValue.getText())) {
			System.out.println(" Package Version Name/version Number is correct");
			logger.info("### Package Version Name / Version Number is correct ###");
		} else {
			System.out.println(" Incorrect Package version name / Version Number ");
			logger.error("### Incorrect Package Version Name / Version Number  ###");
		}
		try {
			installButon.click();
		} catch (NoSuchElementException n) {
			upgradeButton.click();
		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try {
			grantAccessCheckBox.click();
			continueButton.click();
		} catch (NoSuchElementException e) {
			System.out.println(" Grant Access dialog box is not present");
			logger.info("Grant Access dialog box is not present");
		}

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		while (flag == false) {
			try {
				flag = doneButton.isDisplayed();
				//System.out.println(longTimeToInstallText.getText());
				System.out.println(flag);
				if (flag) {
					doneButton.click();
				}
			} catch (NoSuchElementException e) {
				System.out.println(flag);
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			}

		}

		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		while (upgraded == false) {
			count++;
			try {
				upgraded = driver
						.findElement(By.xpath("//th[a[normalize-space(text())='" + packageName
								+ "']]/following-sibling::td[normalize-space(text())='" + versionNumber + "']"))
						.isDisplayed();
				if (upgraded) {
					System.out.println("Package has been Deployed/Upgraded successfuly");
					logger.info("Package has been Deployed/Upgraded successfuly");
				}
				break;
			} catch (NoSuchElementException e) {
				common.refreshPage();
				driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
			}
			
			

		}
		if (upgraded == false) {
			System.out.println("Package Deployement/Upgradation is failled");
			logger.error("Package Deployement/Upgradation is failled");
		}
		
		logger.info("Deployement/upgradation process is completed. ");

	}

}
